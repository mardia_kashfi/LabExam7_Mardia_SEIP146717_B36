<?php
namespace App\SummaryOfOrganization;
use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;


class SummaryOfOrganization extends DB
{
    public $id;
    public $organization_name;
    public $organization_summary;

    public function __construct()
    {
        parent:: __construct();
        if (!isset($_SESSION)) session_start();
    }// end of __construct()


    public function setData($postVariableData=NULL){

        if( array_key_exists("id",$postVariableData) ){

            $this->id     =  $postVariableData['id'];
        }

        if( array_key_exists("input",$postVariableData) ){

            $this->organization_name     =  $postVariableData['input'];
        }

        if( array_key_exists("textarea",$postVariableData) ){

            $this->organization_summary   =  $postVariableData['textarea'];
        }
    }// end of setData()



    public function store(){
        $arrData = array($this->organization_name,$this->organization_summary);
        $sql = "insert into summer_of_organization(organization_name,organization_summary) VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData );


        Utility::redirect('create.php');

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");


        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");






    }// end of store()


}//  end of BookTitle Class
