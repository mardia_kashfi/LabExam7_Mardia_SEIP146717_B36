<?php
namespace App\City;
use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;


class City extends DB{
    public $id;
    public $user_name;
    public $city_name;

    public function setData($postVariableData=NULL){

        if( array_key_exists("id",$postVariableData) ){

            $this->id     =  $postVariableData['id'];
        }

        if( array_key_exists("input1",$postVariableData) ){

            $this->user_name     =  $postVariableData['input1'];
        }

        if( array_key_exists("multiple",$postVariableData) ){

            $this->city_name   =  $postVariableData['multiple'];

            //echo $this->city_name;die();
        }
    }// end of setData()


    public function store(){

     /*  $sql = "insert into city(user_name,city_name) VALUES('$this->user_name', '$this->city_name' )";
        echo $sql;
        die();*/
        $arrData = array($this->user_name,$this->city_name);



        $sql = "insert into city(user_name,city_name) VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData );
        Utility::redirect('create.php');

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


    }// end of store()


}//  end of City Class