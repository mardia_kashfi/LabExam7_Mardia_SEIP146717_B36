<?php
namespace App\Birthday;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;



class Birthday extends DB
{   public $id="";
    public $user_name="";
    public $user_birthday="";
    public function __construct()
    {
        parent::__construct();
    }
    public function setData($postVariableData=NULL){

        if( array_key_exists("id",$postVariableData) ){

            $this->id     =  $postVariableData['id'];
        }

        if( array_key_exists("input",$postVariableData) ){

            $this->user_name     =  $postVariableData['input'];
        }

        if( array_key_exists("date",$postVariableData) ){

            $this->user_birthday   =  $postVariableData['date'];

            //echo $this->city_name;die();
        }
    }// end of setData()


    public function store(){

        /*  $sql = "insert into city(user_name,city_name) VALUES('$this->user_name', '$this->city_name' )";
           echo $sql;
           die();*/
        $arrData = array($this->user_name,$this->user_birthday);



        $sql = "insert into birthday(user_name,user_birthday) VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =$STH->execute($arrData );
        Utility::redirect('create.php');

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


    }// end of store()


}//  end of Gender Class