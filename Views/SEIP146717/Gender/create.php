<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

echo Message::message();



?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Gender - Formoid bootstrap form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body class="blurBg-false" style="background-color:#EBEBEB">



<!-- Start Formoid form-->
<link rel="stylesheet" href="../../../Resourse/assets/gender_files/formoid1/formoid-solid-purple.css" type="text/css" />
<script type="text/javascript" src="../../../Resourse/assets/gender_files/formoid1/jquery.min.js"></script>
<form class="formoid-solid-purple" style="background-color:#FFFFFF;font-size:14px;font-family:'Roboto',Arial,Helvetica,sans-serif;color:#34495E;max-width:580px;min-width:150px" action="store.php" method="post"><div class="title"><h2>Gender</h2></div>
    <div class="element-input"><label class="title"></label><div class="item-cont"><input class="large" type="text" name="input" placeholder="Enter User Name"/><span class="icon-place"></span></div></div>
    <div class="element-radio"><label class="title">Select Gender</label>		<div class="column column1"><label><input type="radio" name="radio" value="Male" /><span>Male</span></label><label><input type="radio" name="radio" value="Female" /><span>Female</span></label></div><span class="clearfix"></span>
    </div>
    <div class="submit"><input type="submit" value="Submit"/></div></form><p class="frmd"><a href="http://formoid.com/v29.php">bootstrap form</a> Formoid.com 2.9</p><script type="text/javascript" src="../../../Resourse/assetsgender_files/formoid1/formoid-solid-purple.js"></script>
<!-- Stop Formoid form-->



</body>
</html>

