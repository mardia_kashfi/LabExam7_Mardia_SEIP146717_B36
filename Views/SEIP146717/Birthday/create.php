
<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

echo Message::message();



?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Birthday</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body class="blurBg-true" style="background-color:#ffffff">



<!-- Start Formoid form-->
<link rel="stylesheet" href="../../../Resourse/assets/birth_files/formoid1/formoid-solid-green.css" type="text/css" />
<script type="text/javascript" src="../../../Resourse/assets/birth_files/formoid1/jquery.min.js"></script>

<form class="formoid-solid-green" action="store.php" style="background-color:#ffffff;font-size:14px;font-family:'Roboto',Arial,Helvetica,sans-serif;color:#34495E;max-width:480px;min-width:150px" method="post"><div class="title"><h2>Birthday</h2></div>

    <div class="element-input"><label class="title"></label><div class="item-cont"><input class="large" type="text" name="input" placeholder="Input Text"/><span class="icon-place"></span></div></div>
    <div class="element-date"><label class="title"></label><div class="item-cont"><input class="large" data-format="yyyy-mm-dd" type="date" name="date" placeholder="Date"/><span class="icon-place"></span></div></div>
    <div class="submit"><input type="submit" value="Create"/></div></form><p class="frmd"><a href="http://formoid.com/v29.php">contact form</a> Formoid.com 2.9</p><script type="text/javascript" src="../../../Resourse/assets/birth_files/formoid1/formoid-solid-green.js"></script>
<!-- Stop Formoid form-->



</body>
</html>
